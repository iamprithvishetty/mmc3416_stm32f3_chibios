/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"
#include "rt_test_root.h"
#include "oslib_test_root.h"

static uint8_t mmc_rec[1]; // Received Data from MMC is stored here
static uint8_t mmc_measurement[4]; // Data to be sent for Measurement are stored here
static volatile uint8_t mmc_values[6]; // X, Y, Z Magnetometer data gets stored here

#define MMC_3416_ADDR 0x30


static const I2CConfig i2ccfg = {
    STM32_TIMINGR_PRESC(15U) |
  STM32_TIMINGR_SCLDEL(4U) | STM32_TIMINGR_SDADEL(2U) |
  STM32_TIMINGR_SCLH(15U)  | STM32_TIMINGR_SCLL(21U),
  0,
  0
};

/*
 * Blinker thread to check if the code is working
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  while (true) {

    //palTogglePad(GPIOA, GPIOA_PIN1); //Comment Out To Check
    chThdSleepMilliseconds(1000);

  }
}

/*
 * I2C Thread to retrieve data
 */
THD_WORKING_AREA(waThread2, 1024);
THD_FUNCTION(Thread2, arg) {
  (void)arg;
  
  chRegSetThreadName("mmc thread");
  uint8_t X_ch_MSB=0x00,Y_ch_MSB=0x00,Z_ch_MSB=0x00,X_ch_LSB=0x00,Y_ch_LSB=0x00,Z_ch_LSB=0x00;
  mmc_measurement[0] = 0x07;
  mmc_measurement[1] = 0x01;
  mmc_measurement[2] = 0x06;
  mmc_measurement[3] = 0x00;

  volatile msg_t mmc_message = MSG_OK;
  volatile i2cflags_t error;  

  while (true) {

    i2cAcquireBus(&I2CD1);
    mmc_message = i2cMasterTransmitTimeout(&I2CD1, MMC_3416_ADDR,mmc_measurement,2,NULL,0,TIME_MS2I(8));
    i2cReleaseBus(&I2CD1);
    if(mmc_message == MSG_OK)
    {
    for(int count=0;count<5;count++)
    {
    palClearPad(GPIOA, GPIOA_PIN1);
    chThdSleepMilliseconds(100);
    palSetPad(GPIOA, GPIOA_PIN1);
    chThdSleepMilliseconds(100);
    }
    }
    else{
      for(int count=0;count<5;count++)
    {
    palClearPad(GPIOA, GPIOA_BUTTON);
    chThdSleepMilliseconds(100);
    palSetPad(GPIOA, GPIOA_BUTTON);
    chThdSleepMilliseconds(100);
    }
    }

    if(mmc_message == MSG_OK)
    {
    i2cAcquireBus(&I2CD1);
    mmc_message = i2cMasterTransmitTimeout(&I2CD1, MMC_3416_ADDR,mmc_measurement+2,1,NULL,0,TIME_MS2I(8));
    i2cReleaseBus(&I2CD1);
    if(mmc_message == MSG_OK)
    {
    for(int count=0;count<5;count++)
    {
    palClearPad(GPIOA, GPIOA_PIN1);
    chThdSleepMilliseconds(100);
    palSetPad(GPIOA, GPIOA_PIN1);
    chThdSleepMilliseconds(100);
    }
    }
    else{
      for(int count=0;count<5;count++)
    {
    palClearPad(GPIOA, GPIOA_BUTTON);
    chThdSleepMilliseconds(100);
    palSetPad(GPIOA, GPIOA_BUTTON);
    chThdSleepMilliseconds(100);
    }
    }
    }

    mmc_rec[0] = 0;
    if( mmc_message == MSG_OK)
    {
      while(!(*mmc_rec & 0x01 ))
      {
        i2cAcquireBus(&I2CD1);
        mmc_message = i2cMasterReceiveTimeout(&I2CD1, MMC_3416_ADDR,mmc_rec,1,TIME_MS2I(8));
        i2cReleaseBus(&I2CD1);
        palClearPad(GPIOA, GPIOA_PIN2);
        chThdSleepMilliseconds(500);
        palSetPad(GPIOA, GPIOA_PIN2);
        chThdSleepMilliseconds(500);
          if(mmc_message == MSG_OK)
          {
            for(int count=0;count<5;count++)
            {
            palClearPad(GPIOA, GPIOA_PIN1);
            chThdSleepMilliseconds(100);
            palSetPad(GPIOA, GPIOA_PIN1);
            chThdSleepMilliseconds(100);
            }
          }
          else{
              for(int count=0;count<5;count++)
            {
            palClearPad(GPIOA, GPIOA_BUTTON);
            chThdSleepMilliseconds(100);
            palSetPad(GPIOA, GPIOA_BUTTON);
            chThdSleepMilliseconds(100);
            }

          }
              
      }

  }

  if(mmc_message == MSG_OK)
  {
    i2cAcquireBus(&I2CD1);
    mmc_message = i2cMasterTransmitTimeout(&I2CD1, MMC_3416_ADDR,mmc_measurement+3,1,NULL,0,TIME_MS2I(8));
    i2cReleaseBus(&I2CD1);
    if(mmc_message == MSG_OK)
      {
        for(int count=0;count<5;count++)
        {
        palClearPad(GPIOA, GPIOA_PIN1);
        chThdSleepMilliseconds(100);
        palSetPad(GPIOA, GPIOA_PIN1);
        chThdSleepMilliseconds(100);
        }
      }
      else{
          for(int count=0;count<5;count++)
        {
        palClearPad(GPIOA, GPIOA_BUTTON);
        chThdSleepMilliseconds(100);
        palSetPad(GPIOA, GPIOA_BUTTON);
        chThdSleepMilliseconds(100);
        }

      }
  }

  if( mmc_message == MSG_OK)
  {
    i2cAcquireBus(&I2CD1);
    mmc_message = i2cMasterReceiveTimeout(&I2CD1, MMC_3416_ADDR,mmc_values,6,TIME_MS2I(8));
    i2cReleaseBus(&I2CD1);
    if(mmc_message == MSG_OK)
      {
        for(int count=0;count<5;count++)
        {
        palClearPad(GPIOA, GPIOA_PIN1);
        palClearPad(GPIOA, GPIOA_BUTTON);
        chThdSleepMilliseconds(300);
        palSetPad(GPIOA, GPIOA_PIN1);
        palSetPad(GPIOA, GPIOA_BUTTON);
        chThdSleepMilliseconds(300);
        }
      }
      else{
          for(int count=0;count<5;count++)
        {
        palClearPad(GPIOA, GPIOA_BUTTON);
        chThdSleepMilliseconds(300);
        palSetPad(GPIOA, GPIOA_BUTTON);
        chThdSleepMilliseconds(300);
        }

      }
  }
}
}

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
*/
  halInit();
  chSysInit();
  i2cInit();
  i2cStart(&I2CD1, &i2ccfg);

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  chThdCreateStatic(waThread2, sizeof(waThread2), NORMALPRIO, Thread2, NULL);

  /* This is now the idle thread loop, you may perform here a low priority
     task but you must never try to sleep or wait in this loop. Note that
     this tasks runs at the lowest priority level so any instruction added
     here will be executed after all other tasks have been started.*/
  while (true) {
    chThdSleepMilliseconds(1000);
  }
}


